<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Cuenta;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Helper\ResponseBuilder;

class ClienteController extends BaseController
{
    public function index(Request $request){
    	$clientes = Cliente::all();
    	return response()->json($clientes,200);
    }
    public function getCliente(Request $request,$cedula){
    if($request->isjson()){


    	$cliente = Cliente::where('cedula',$cedula)->get();
    	if(!$cliente->isEmpty()){
    		$status=true;
    		$info="Data is listed succesfully";

    	}else{
    		$status=false;
    		$info="Data is not listed succesfully";
    	}
    	return ResponseBuilder::result($status,$info,$cliente);
    }else{
	$status=false;
    $info="Unautorized";
}
	return ResponseBuilder::result($status,$info);
}
public function getapellido(Request $request,$apellidos){
    if($request->isjson()){


    	$cliente = Cliente::where('apellidos',$apellidos)->get();
    	if(!$cliente->isEmpty()){
    		$status=true;
    		$info="Data is listed succesfully";

    	}else{
    		$status=false;
    		$info="Data is not listed succesfully";
    	}
    	return ResponseBuilder::result($status,$info,$cliente);
    }else{
	$status=false;
    $info="Unautorized";
}
	return ResponseBuilder::result($status,$info);
}
#crear cliente
public function createCliente(Request $request){
	$cliente= new Cliente();
	$cliente->cedula = $request->cedula;
	$cliente->nombres = $request->nombres;
	$cliente->apellidos = $request->apellidos;
	$cliente->genero = $request->genero;
	$cliente->estadoCivil = $request->estadoCivil;
	$cliente->fechaNacimiento = $request->fechaNacimiento;
	$cliente->correo = $request->correo;
	$cliente->telefono = $request->telefono;
	$cliente->celular = $request->celular;
	$cliente->direccion = $request->direccion;

	$cliente->save();

	$cuenta= new Cuenta();
	$cuenta->numero = $request->numero;
	$cuenta->estado = $request->estado;
	$cuenta->fechaApertura = $request->fechaApertura;
	$cuenta->tipoCuenta = $request->tipoCuenta;
	$cuenta->saldo = $request->saldo;
	$cuenta->cliente_id = $cliente->id;
	$cuenta->save();
	return response()->json($cliente);
}
#crear cuenta
public function createCuenta(Request $request){
	$cuenta= new Cuenta();
	$cuenta->numero = $request->numero;
	$cuenta->estado = $request->estado;
	$cuenta->fechaApertura = $request->fechaApertura;
	$cuenta->tipoCuenta = $request->tipoCuenta;
	$cuenta->saldo = $request->saldo;
	$cuenta->cliente_id = $request->cliente_id;
	$cuenta->save();
	return response()->json($cuenta);




	

	$cliente->save();
	return response()->json($cliente);

}

public function actualizarCliente(Request $request,$cedula){
	$cliente = Cliente::where('cedula',$cedula)->first();
	if(!empty($cliente)){
		$cliente->nombres = $request->nombres;
		$cliente->apellidos = $request->apellidos;
		$cliente->genero = $request->genero;
		$cliente->estadoCivil = $request->estadoCivil;
		$cliente->fechaNacimiento = $request->fechaNacimiento;
		$cliente->correo = $request->correo;
		$cliente->telefono = $request->telefono;
		$cliente->celular = $request->celular;
		$cliente->direccion = $request->direccion;

		$cliente->save();
		$status=true;
		$info="Modificacion realizada";
		}else{
	    		$status=false;
	    		$info="Data is not listed succesfully";
	    	}
    return ResponseBuilder::result($status,$info,$cliente);
	

}

public function actualizarCuenta(Request $request,$numero){
	$cuenta = Cuenta::where('numero',$numero)->first();
	if(!empty($cuenta)){
		$cuenta->tipoCuenta = $request->tipoCuenta;
		$cuenta->estado = $request->estado;
		$cuenta->fechaApertura= $request->fechaApertura;
		
		

		$cuenta->save();
		$status=true;
		$info="Modificacion realizada";
		}else{
	    		$status=false;
	    		$info="Data is not listed succesfully";
	    	}
    return ResponseBuilder::result($status,$info,$cuenta);
	

}
}
