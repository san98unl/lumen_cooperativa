<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/prueba','PruebaController@index');
$router->group(['prefix'=>'clientes'],function($router){
	$router->get('','ClienteController@index');	
	$router->get('/all','ClienteController@index');
	$router->get('/get/{cedula}','ClienteController@getCliente');
	$router->get('/gets/{apellidos}','ClienteController@getapellido');
	$router->post('','ClienteController@createCliente');
	$router->post('/create_cuenta','ClienteController@createCuenta');
	$router->post('/delete_cuenta','ClienteController@deleteCuenta');

	$router->post('/update/{cedula}','ClienteController@actualizarCliente');
	$router->post('/update/cuenta/{numero}','ClienteController@actualizarCuenta');

});
#grupo para logi y logout
$router->group(['prefix'=>'usuarios'],function($router){
	$router->post('/ingresar','UserController@login');	

});

$router->group(['prefix'=>'transaccion'],function($router){
	$router->post('/deposito_retiro','TransaccionController@realizar_transaccion');


});
