<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Cuenta;
use App\User;
use App\Transaccion;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;


class TransaccionController extends BaseController
{
	
	public function realizar_transaccion(Request $request){
		#if($request->isjson()){


    	$cuenta=Cuenta::where('numero',$request->numero)->get();
    	$transaccion=new Transaccion();

	    	if(!$cuenta->isEmpty()){

	    		$transaccion->fecha=$request->fecha;
	    		$transaccion->tipo=$request->tipo;	
	    		



	    		if ($transaccion->tipo=='deposito') {
	    			# code...
	    			$valor=$request->valor;
	    			$cuenta[0]->saldo=$cuenta[0]->saldo+$valor;
	    			

	    			$transaccion->valor=$valor;
	    			$transaccion->descripcion=$request->descripcion;
	    		
		    		$transaccion->responsable=$request->responsable;
		    		$transaccion->cuenta_id=$cuenta[0]->cuenta_id;
		    		$cuenta[0]->save();
		    		$transaccion->save();
		    		$status=true;
		    		$info="Deposoto realizado";
	    			
	    		}elseif ($transaccion->tipo=='retiro') {
	    			# code...
	    			$valor=$request->valor;
	    			$cuenta[0]->saldo=$cuenta[0]->saldo-$valor;
	    			

	    			$transaccion->valor=$valor;
	    			$transaccion->descripcion=$request->descripcion;
	    		
		    		$transaccion->responsable=$request->responsable;
		    		$transaccion->cuenta_id=$cuenta[0]->cuenta_id;
		    		$cuenta[0]->save();
		    		$transaccion->save();
		    		$status=true;
		    		$info="Deposto realizado";
	    		}
	    		
	    		



	    	}else{
	    		$status=false;
	    		$info="Data is not listed succesfully";
	    	}
    	return ResponseBuilder::result($status,$info,$transaccion);
	   # }else{
		#$status=false;
	    #$info="Unautorized";
	#}
	}
		}